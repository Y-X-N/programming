#include <graphics.h>
#include <conio.h>
#include <math.h>

#define High 480  
#define Width 640

int main()
{
	initgraph(Width, High);			
	int center_x,center_y;     
	center_x = Width/2;
	center_y = High/2;
	int secondLength = Width/5;           
	
	int secondEnd_x,secondEnd_y;   
		secondEnd_x = center_x + secondLength;
		secondEnd_y = center_y;		
		
		setlinestyle(PS_SOLID, 2);  
		setcolor(WHITE);
		line(center_x, center_y, secondEnd_x, secondEnd_y);
	
	getch();			
	closegraph();			
	return 0;
}
