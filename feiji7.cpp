# include <stdio.h>
# include <stdlib.h>
# include <conio.h>
# include <windows.h>
# define High 25
# define Width 50
int position_x, position_y;
int canvas[High][Width]={0}; 

int high,width;
int score;
void startup()
{

	position_x=high/2;
	position_y=width/2;
	canvas[position_x][position_y]=1;
}
void gotoxy(int x,int y)
{
	HANDLE handle=GetStdHandle(STD_OUTPUT_HANDLE);
	COORD pos;
	pos.X=x;
	pos.Y=y;
	SetConsoleCursorPosition(handle,pos);
}

void show()
{
	gotoxy(0,0); 
	int i,j;
	for (i=0;i<High;i++)
	{
		for (j=0;j<Width;j++)
		{
			if(canvas[i][j]==0)
			printf(" ");
			else if (canvas[i][j]==1)
			printf ("*");
		}
		printf ("\n");
	}

}
void updateWithoutInput()
{
}
void updateWithInput()
{
	char input;
	if(kbhit())
	{
		input = getch();
		if(input=='a')
		{
		canvas[position_x][position_y]=0;
		position_y--;
		canvas[position_x][position_y]=1;
		}
		else if(input=='d')
		{
			canvas[position_x][position_y]=0;
			position_y++;
			canvas[position_x][position_y]=1;
		}
		else if(input=='w')
		{
			canvas[position_x][position_y]=0;
			position_x--;
			canvas[position_x][position_y]=1;
		}
		else if(input=='s')
		{
			canvas[position_x][position_y]=0;
			position_x++;
			canvas[position_x][position_y]=1;
		}
	}
}
int main()
{
	startup();
	while(1)
	{
		show();
		updateWithoutInput();
		updateWithInput();
	}
	return 0;
}
