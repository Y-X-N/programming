#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <windows.h>

#define High 15 
#define Width 25

int position_x,position_y;
int enemy_x,enemy_y;
int canvas[High][Width] = {0};

int score; 

void gotoxy(int x,int y)
{
    HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD pos;
    pos.X = x;
    pos.Y = y;
    SetConsoleCursorPosition(handle,pos);
}

void startup()
{
	position_x = High-1;
	position_y = Width/2;
	canvas[position_x][position_y] = 1;	
		enemy_x=0;
		enemy_y=position_y;
		canvas[enemy_x][enemy_y] = 3;
	score = 0;

}

void show() 
{
	gotoxy(0,0);
	int i,j;
	for (i=0;i<High;i++)
	{
		for (j=0;j<Width;j++)
		{
			if (canvas[i][j]==0)
				printf(" ");  
			else if (canvas[i][j]==1)
				printf("*"); 
			else if (canvas[i][j]==2)
				printf("|");
			else if (canvas[i][j]==3)
				printf("@");
		}
		printf("\n");
	}
	printf("�÷֣�%3d\n",score);
	Sleep(20);
}	

void updateWithoutInput()
{
	int i,j;
	for (i=0;i<High;i++)
	{
		for (j=0;j<Width;j++)
		{
			if (canvas[i][j]==2) 
			{
					if ((i==enemy_x) && (j==enemy_y)) 
					{
						score++;
						canvas[enemy_x][enemy_y] = 0;
     					enemy_x=0;
     					enemy_y=rand()%Width;
						canvas[enemy_x][enemy_y] = 3;
						canvas[i][j] = 0;  
					}
				canvas[i][j] = 0;
				if (i>0)
					canvas[i-1][j] = 2;
			}
		}
	}

		if ((position_x==enemy_x) && (position_y==enemy_y)) 
		{
			printf("ʧ�ܣ�\n");
			Sleep(3000);
			system("pause");
			exit(0);
		}

		if (enemy_x>High) 
		{
			canvas[enemy_x][enemy_y] = 0;
			enemy_x = 0;      
			enemy_y = rand()%Width;
			canvas[enemy_x][enemy_y] = 3;
			score--; 
		}
		static int speed=0;
		if(speed<10)
		speed++;
		if (speed = 10 )
		{
				canvas[enemy_x][enemy_y] = 0;
				enemy_x++;			
				speed = 0;
				canvas[enemy_x][enemy_y] = 3;
			
		}
}

void updateWithInput() 
{
	char input;
	if(kbhit()) 
	{
		input = getch(); 
		if (input == 'a' && position_y>0) 
		{
			canvas[position_x][position_y] = 0;
			position_y--; 
			canvas[position_x][position_y] = 1;
		}
		else if (input == 'd' && position_y<Width-1)
		{
			canvas[position_x][position_y] = 0;
			position_y++; 
			canvas[position_x][position_y] = 1;
		}
		else if (input == 'w')
		{
			canvas[position_x][position_y] = 0;
			position_x--; 
			canvas[position_x][position_y] = 1;
		}
		else if (input == 's')
		{
			canvas[position_x][position_y] = 0;
			position_x++;  
			canvas[position_x][position_y] = 1;
		}
		else if (input == ' ')  
		{
				canvas[position_x-1][position_y] = 2; 
		}		
	}
}

int main()
{
	startup(); 	
	while (1) 
	{
		show(); 
		updateWithoutInput();  
		updateWithInput(); 
	}
	return 0;
}
