#include <graphics.h>
#include <conio.h>
#include <math.h>

#define High 480  
#define Width 640
#definPI 3.14159

int main()
{
	initgraph(Width, High);			
	int center_x,center_y;     
	center_x = Width/2;
	center_y = High/2;
	int secondLength = Width/7; 
	int minuteLength = Width/6;
	int hourLength = Width/5;          
	
	int secondEnd_x,secondEnd_y;
	int minuteEnd_x,minuteEnd_y; 
	int hourEnd_x,hourEnd_y; 
	float seconAngle;
	float minuteAngle;
	float hourAngle
	SYSTEMTIME ti; 
	while(1)
{
	GetLocalTime(&ti);
	secondAngle = ti.wSecond * 2*PI/60;
	minuteAngle = ti.wMinute * 2*PI/60;
	hourAngle = ti.wHour * 2*PI/12;
		secondEnd_x = center_x + secondLength*sin(secondAngle);
		secondEnd_y = center_y - secondLength*cos(secondAngle);	
		minuteEnd_x = center_x + minuteLength*sin(minuteAngle);
		minuteEnd_y = center_y - minuteLength*cos(minuteAngle);
		hourEnd_x = center_x + hourLength*sin(hourAngle);
		hourEnd_y = center_y - hourLength*cos(hourAngle);
		
		setlinestyle(PS_SOLID, 2);  
		setcolor(WHITE);
		line(center_x, center_y, secondEnd_x, secondEnd_y);
		
		setlinestyle(PS_SOLID, 4);  
		setcolor(BLUE);
		line(center_x, center_y, minuteEnd_x, minuteEnd_y);
		
		setlinestyle(PS_SOLID, 6);  
		setcolor(RED);
		line(center_x, center_y, hourEnd_x, hourEnd_y);
		
		sleep(10);
		
		setcolor(BLACK);
		setlinestyle(PS_SOLID, 2); 
		line(center_x, center_y, secondEnd_x, secondEnd_y);
		setlinestyle(PS_SOLID, 4);  
		line(center_x, center_y, minuteEnd_x, minuteEnd_y);
		setlinestyle(PS_SOLID, 6);  
		line(center_x, center_y, hourEnd_x, hourEnd_y);
}
	getch();			
	closegraph();			
	return 0;
}
