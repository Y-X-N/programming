#include <conio.h>
#include <graphics.h>
#include<windows.h>
#define High 480 
#define Width 640

int ball1_x,ball1_y; 
int bal2l_x,ball2_y;
int radius;

void startup() 
{
	ball1_x = Width/3;
	ball1_y = High/3;
	ball2_x = Width*2/3;
	ball2_y = High*2/3;
	radius = 20;

	initgraph(Width,High);
	BeginBatchDraw();
}

void clean()
{
	setcolor(BLACK);
	setfillcolor(BLACK);
	fillcircle(ball1_x, ball1_y, radius); 	
	fillcircle(ball2_x, ball2_y, radius); 
}	

void show()
{
	setcolor(GREEN);
	setfillcolor(GREEN);
	fillcircle(ball1_x, ball1_y, radius);
	
	setcolor(RED);
	setfillcolor(RED);
	fillcircle(ball2_x, ball2_y, radius);
	FlushBatchDraw();

	Sleep(3);	
}	

void updateWithoutInput()
{
}

void updateWithInput()
{	
	char input;
	if(kbhit())
	{
		input=getch();
		int step=10;
		if(input=='a')
		ball1_x-=step;
		if(input=='d')
		ball1_x+=step;
		if(input=='w')
		ball1_y-=step;
		if(input=='s')
		ball1_y+=step;
		
		if(input=='4')
		ball2_x-=step;
		if(input=='6')
		ball2_x+=step;
		if(input=='8')
		ball2_y-=step;
		if(input=='5')
		ball2_y+=step;
	}
}

void gameover()
{
	EndBatchDraw();
	closegraph();
}

int main()
{
	startup(); 	
	while (1) 
	{
		clean();  
		updateWithoutInput(); 
		updateWithInput(); 
		show();
	}
	gameover();
	return 0;
}
