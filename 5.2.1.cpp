#include <graphics.h>
#include <conio.h>

// 引用 Windows Multimedia API
#pragma comment(lib,"Winmm.lib")

#define High 800  // 游戏画面尺寸
#define Width 590

IMAGE img_bk; // 背景图片
float position_x,position_y; // 飞机位置
IMAGE img_planeNormal1,img_planeNormal2; // 飞机图片

void startup()
{
	loadimage(&img_bk, "D:\\background.jpg");
	loadimage(&img_planeNormal1, "D:\\planeNormal_1.jpg");
	loadimage(&img_planeNormal2, "D:\\planeNormal_2.jpg");
	position_x = Width*0.5;
	position_y = High*0.7;
	BeginBatchDraw();	
}
void show()
{
	putimage(0, 0, &img_bk);	// 显示背景	
	putimage(position_x-50, position_y-60, &img_planeNormal1,NOTSRCERASE); // 显示飞机	
	putimage(position_x-50, position_y-60, &img_planeNormal2,SRCINVERT);
	FlushBatchDraw();
	Sleep(2);
}

void updateWithoutInput()
{
}
void updateWithInput()
{
	MOUSEMSG m;		// 定义鼠标消息
	while (MouseHit())  //这个函数用于检测当前是否有鼠标消息
				m = GetMouseMsg();
			if(m.uMsg == WM_MOUSEMOVE)
			{
				// 飞机的位置等于鼠标所在的位置
				position_x = m.x;
				position_y = m.y;			
			}
		}
	}
	void gameover()
{
	EndBatchDraw();
	getch();
	closegraph();
}
int main()
{
	startup();  // 数据初始化	
	while (1)  //  游戏循环执行
	{
		show();  // 显示画面
		updateWithoutInput();  // 与用户输入无关的更新
		updateWithInput();     // 与用户输入有关的更新
	}
	gameover();     // 游戏结束、后续处理
	return 0;
}
